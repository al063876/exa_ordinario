
package exa_ordinario;

/**
 *
 * @author danie
 */

public class Arbol_Examen {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        int dato;
        String nombre;
        ArbolBinario_Examen miArbol = new ArbolBinario_Examen();
        miArbol.agregarNodo(82, "R");
        miArbol.agregarNodo(73, "I");
        miArbol.agregarNodo(67, "C");
        miArbol.agregarNodo(65, "A");
        miArbol.agregarNodo(82, "R");
        miArbol.agregarNodo(68, "D");
        miArbol.agregarNodo(79, "O");
        miArbol.agregarNodo(78, "N");
        miArbol.agregarNodo(69, "E");
        miArbol.agregarNodo(76, "L");
        miArbol.agregarNodo(83, "S");
        miArbol.agregarNodo(72, "H");
        miArbol.agregarNodo(90, "Z");
        
        System.out.println("PostOrden");
        System.out.println("SANCHEZ ROSARIO RICARDO DANIEL, 63876 ");
        if (!miArbol.estaVacio()) {
            miArbol.PostOrden(miArbol.raiz);
        }
    }
}
