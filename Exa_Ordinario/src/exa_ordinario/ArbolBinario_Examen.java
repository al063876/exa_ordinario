/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exa_ordinario;

/**
 *
 * @author danie
 */
public class ArbolBinario_Examen {
    
NodoArbol_Examen raiz;

    public ArbolBinario_Examen() {
        raiz = null;
    }

    public void agregarNodo(int d, String nom) {
        NodoArbol_Examen nuevo = new NodoArbol_Examen(d, nom);
        if (raiz == null) {
            raiz = nuevo;
        } else {
            NodoArbol_Examen auxiliar = raiz;
            NodoArbol_Examen padre;
            while (true) {
                padre = auxiliar;
                if (d < auxiliar.dato) {
                    auxiliar = auxiliar.hijoizquierdo;
                    if (auxiliar == null) {
                        padre.hijoizquierdo = nuevo;
                        return;
                    }
                } else {
                    auxiliar = auxiliar.hijoderecho;
                    if (auxiliar == null) {
                        padre.hijoderecho = nuevo;
                        return;
                    }
                }
            }
        }
    }

    public boolean estaVacio() {
        return raiz == null;
    }

    public void PostOrden(NodoArbol_Examen r) {       
        if (r != null) {
            PostOrden(r.hijoizquierdo);
            PostOrden(r.hijoderecho);
            System.out.println(r.dato);           
        }
    }
}